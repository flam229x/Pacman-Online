#ifndef INET_SERV_H
#define INET_SERV_H

#include <arpa/inet.h>
#include <stdio.h>

#include "../update/proc.h"
#include "../update/system.h"
#include "../bidir_list/list.h"
#include "game.h"

void init_server(const struct sockaddr_in *addr);

void wait_clients();

int check_name(char *name);

void send_map(int fd);

void start_game(uint32_t frame_delay, crd_t player_crd);

int name_to_fd(char *name);

char *fd_to_name(int fd);

void delete_name(char *name, node_t **last);

void disconnect(int fd);

void send_new_state(int fd, uint8_t dir);

#endif
