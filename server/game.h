#ifndef GAME_SERV_H
#define GAME_SERV_H

#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include "../update/system.h"
#include "../bidir_list/list.h"


void gen_walls();

// -1 - if not found; >=0 - number neigh; -2 - if too closed in border
int8_t check_wall(uint8_t x, uint8_t y); 

//ret player's coord
crd_t create_qmap();

//fill array players start positions
void gen_start_crd(crd_t f_player, crd_t pl_crd[]);

#endif
