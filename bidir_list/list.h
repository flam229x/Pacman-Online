#ifndef LINKLIST_H
#define LINKLIST_H

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
    void *data;
    struct node *next;
    struct node *prev;
} node_t;

node_t *insert_node(node_t *afternode, void *data);

void delete_node(node_t *list, node_t **last);

void init_list(node_t **root);

void free_list(node_t *root);

void delete_node_data(long size_data, void *data, node_t **root);

#endif

