CC=gcc
CFLAGS=-Wall -Werror -g
BIN=pacman.exe

upd=update
cli=client
ser=server
lis=bidir_list

OBJ=$(upd)/proc.o $(cli)/game.o $(cli)/inet.o $(lis)/list.o main.o

all: tlist tupdate tserver tclient

main.o: main.c
	$(CC) $(CFLAGS) -c -o $@ $<

tclient: main.o
	make -C $(cli)
	$(CC) $(CFLAGS)  -o $(BIN) $(OBJ) -lncurses

tupdate:
	make -C $(upd)

tserver:
	make -C $(ser)

tlist:
	make -C $(lis)

clean:
	make clean -C $(ser)
	make clean -C $(cli)
	make clean -C $(lis)
	make clean -C $(upd)
	rm -rf *.o *.exe *.log
