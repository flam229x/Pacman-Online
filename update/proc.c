#include "proc.h"

int Socket(int domain, int type, int protocol)
{

    int sock;
    if ((sock = socket(domain, type, protocol)) == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    return sock;
}


void Connect(int socket, const struct sockaddr *address,
           socklen_t address_len)
{
    int status;
    if ((status = connect(socket, address, address_len)) == -1)
    {
        perror("connect");
        exit(EXIT_FAILURE);
    }
 
}


in_addr_t Inet_addr(const char *cp)
{
    in_addr_t ip;

    if ((ip = inet_addr(cp)) == INADDR_NONE)
    {
        fprintf(stderr, "inet_pton: error\n");
        exit(EXIT_FAILURE);
    }

    return ip;
}


ssize_t Recv(int socket, void *buffer, size_t length, int flags)
{
    size_t read_bytes;
    if ((read_bytes = recv(socket, buffer, length, flags)) == -1)
    {
        perror("recv");
    }

    return read_bytes;
}


ssize_t Send(int socket, const void *buffer, size_t length, int flags)
{
    size_t sent_bytes;
    if ((sent_bytes = send(socket, buffer, length, flags)) == -1)
    {
        perror("send");
    }

    return sent_bytes;
}


int Bind(int socket, const struct sockaddr *address,
           socklen_t address_len)
{
    int ret;
    if ((ret = bind(socket, address, address_len)) == -1)
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    return ret;
}


int Listen(int socket, int backlog)
{
    int ret;
    if ((ret = listen(socket, backlog)) == -1)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    return ret;
}


int Accept(int socket, struct sockaddr *address, socklen_t *address_len)
{
    int ret;
    if ((ret = accept(socket, address, address_len)) == -1)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    return ret;
}


void Pselect(int nfds, fd_set *readfds, const struct timespec *timeout)
{
    if (pselect(nfds, readfds, NULL, NULL, timeout, NULL) == -1)
    {
        perror("select");
        exit(__LINE__);
    }
}

int Close(int fd)
{
    int ret;
    if ((ret = close(fd)) != 0)
    {
        perror("close() error");
        exit(__LINE__);
    }

    return ret;
}


int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
{
    int ret;
    if ((ret = select(nfds, readfds, writefds, exceptfds, timeout)) == -1)
    {
        perror("select() error");
        exit(__LINE__);
    }

    return ret;
}


pid_t Fork(void)
{
    pid_t ret;

    if ((ret = fork()) == -1)
    {
        perror("fork() error");
        exit(__LINE__);
    }

    return ret;
}

