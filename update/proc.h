#ifndef PROC_H
#define PROC_H

#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <time.h>
#include <sys/select.h>
#include <unistd.h>
#include <pthread.h>


int Socket(int domain, int type, int protocol); 

void Connect(int socket, const struct sockaddr *address,
           socklen_t address_len);

in_addr_t Inet_addr(const char *cp);

ssize_t Recv(int socket, void *buffer, size_t length, int flags);

ssize_t Send(int socket, const void *buffer, size_t length, int flags);

int Bind(int socket, const struct sockaddr *address,
           socklen_t address_len);

int Listen(int socket, int backlog);

int Accept(int socket, struct sockaddr *address,
   socklen_t *address_len);

void Pselect(int nfds, fd_set *readfs, const struct timespec *timeout);

int Select(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);

// int Pthread_create(pthread_t *thread, const pthread_attr_t *attr,
//                     void *(*start_routine) (void *arg), void *arg);

pid_t Fork(void);

int Close(int fd);

#endif

