#ifndef SYSTEM_H
#define SYSTEM_H

#include <inttypes.h>

#define PLAYER_PAIR 1
#define ENEMY_PAIR 2

#define MAX_WALLS 100
#define HEIGHT_AREA 15
#define WIDTH_AREA 20

#define START_BORDER_X 0
#define START_BORDER_Y 0
#define START_MAP_X 1
#define START_MAP_Y 1

#define TRIES 100
#define ANCHORS (MAX_WALLS/5)

#define FOOD 0xAA
#define WALL 0xFF
#define PLAYER 0x22
#define SPACE 0x00

#define SIZE_ARG 255
#define MAX_NAME_LEN 255
#define MAGIC 0xabcdfe01

#define MAX(a, b) ((a) > (b)) ? (a) : (b)
#define MIN(a, b) ((a) > (b)) ? (b) : (a)

#define MAX_PLAYERS 4

#define UP 0x00
#define RIGHT 0x01
#define DOWN 0x02
#define LEFT 0x03

#define UP_ST '^'
#define DOWN_ST 'v'
#define RIGHT_ST '>'
#define LEFT_ST '<'

#define FOOD_CH '.'
#define WALL_CH '#'
#define SPACE_CH ' '

#define HELP_SERVER """-h \t Print help mess\n"""\
"""----------SERVER----------\n"""\
"""-d \t Frame delay mseconds (by default 300)\n"""\
"""-n \t Number of clients (by default 4)\n"""\
"""-p \t Port (by default 2229)\n"""\
"""-f \t Path to log file (by default stdin)\n"""

#define HELP_CLIENT """-h \t Print help mess\n"""\
"""---------CLIENT----------\n"""\
"""-p \t Port (by default 2229)\n"""\
"""-a \t Address of server (by default localhost)\n"""

typedef struct crd
{
    uint8_t x;
    uint8_t y;
} __attribute__((packed)) crd_t;

typedef struct wall
{
    crd_t crd;
    uint8_t n_neigh;
} __attribute__((packed)) wall_t;


typedef struct player
{
    uint32_t start_x;
    uint32_t start_y;
    uint32_t start_direction;
    uint32_t player_name_len;
    char player_name[MAX_NAME_LEN+1];
} __attribute__((packed)) player_t;


typedef struct ex_player
{
    struct player pl;
    uint32_t score;
} __attribute__((packed)) ex_player_t;


typedef struct player_serv
{
    char player_name[MAX_NAME_LEN+1];
    uint32_t player_name_len;
    int fd;
} __attribute__((packed)) player_serv_t;


typedef struct mess
{
    uint32_t magic;
    uint32_t ptype;
    uint32_t datasize;
} __attribute__((packed)) mess_t;

#endif
