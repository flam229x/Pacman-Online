#ifndef INET_CLIENT_H
#define INET_CLIENT_H

#include <arpa/inet.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "../update/proc.h"
#include "../update/system.h"
#include "../bidir_list/list.h"

void connect_to_server(const struct sockaddr_in *addr, char *name);

void get_map(uint8_t *q_map);

void ready();

ex_player_t *start_game(char *cur_name, uint32_t *frame_delay, uint32_t *player_count);

int get_state();

void send_state(char *name, uint8_t dir);

#endif
