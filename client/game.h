#ifndef GAME_CLIENT_H
#define GAME_CLIENT_H

#include <ncurses.h>

#include "../update/system.h"
#include "../bidir_list/list.h"

void expand_map();

void print_map(int x_start, int y_start);

void draw_map();

void print_border(int x_start, int y_start);

void print_players(ex_player_t *cur_player);

void move_players(char *name, uint8_t dir);

void itoa(char *buf, int num);

void print_score(int y, int x, ex_player_t *player);

void counting_food(uint32_t num_players);

int check_end_game();

void end_game();

int check_collision_players(char *name, uint32_t new_y, uint32_t new_x);

#endif
