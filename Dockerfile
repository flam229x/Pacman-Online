FROM ubuntu:latest

RUN apt-get update && apt-get install -y bash make gcc libncurses5-dev libncursesw5-dev

#RUN pacman --noconfirm -Syu && pacman --noconfirm -S bash make gcc ncurses

COPY . ./app

WORKDIR /app

RUN make


